#include <iostream>

struct Password
{
	char value[16];
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl; 
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********")) 
		// put break point in line 18 and when it stops you change the incorrect value from 
		//true to false and you can enter any password you want and it will write congrats
		pwd.incorrect = false;

	if(!pwd.incorrect)
		std::cout << "Congratulations\n";

	return 0;
}
